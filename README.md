Unless otherwise stated, all policy documents in the DrugPeace project are copyright by their author.
Unless otherwise stated, all DrugPeace documents are available under a CreativeCommons-Attribution-ShareAlike 4.0 license. 
If you alter DrugPeace documents in any way, please give your name as the author of the altered version, and a link back to the original.